# Stage 1: Building the application
FROM ruby:3.0.2 as Builder

# Install dependencies
RUN apt-get update -qq && apt-get install -y postgresql-client redis-server

# Set the working directory
WORKDIR /app/hello_world_app

# Copy the Gemfile and Gemfile.lock
COPY ./hello_world_app/ .
# COPY ./hello_world_app/Gemfile .
# COPY ./hello_world_app/Gemfile.lock .

# Install gems
RUN bundle install

CMD ["rails", "server"]
