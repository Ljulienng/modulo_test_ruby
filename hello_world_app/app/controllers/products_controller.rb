class ProductsController < ApplicationController
	def index
		@products = Product.all
	end

	def create
		@product = Product.new(product_params)
		@product.save
		redirect_to products_path
	end

	def show
		@product = Product.find(params[:id])
	end
end
