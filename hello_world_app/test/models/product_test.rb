# test/models/product_test.rb

require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  setup do
    Rails.application.load_seed # This line loads your seeds.rb file
  end

  test "should have seeded products" do
    assert Product.exists?(name: 'PowerPanne'), "Helps people who crashes their cars"
    assert Product.exists?(name: 'Modul home'), "Domotic system for your home"
  end
end
