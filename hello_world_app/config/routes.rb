Rails.application.routes.draw do
  get 'product/index'
  resources :products
  root 'welcome#index'
end
